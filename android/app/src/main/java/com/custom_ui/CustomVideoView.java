package com.custom_ui;

import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

public class CustomVideoView extends SimpleViewManager<VideoView> {
    @NonNull
    @Override
    public String getName() {
        return "CustomVideoView";
    }
    
    @NonNull
    @Override
    protected VideoView createViewInstance(@NonNull ThemedReactContext reactContext) {
        return new VideoView(reactContext);
    }
    
    @ReactProp(name="url")
    public void setVideoPath(VideoView videoView, String path){
        Uri uri = Uri.parse(path);
        videoView.setVideoURI(uri);
        videoView.start();
        try {
            MediaController mediacontroller = new MediaController(videoView.getContext());
            mediacontroller.setAnchorView(videoView);
        
           
            videoView.setMediaController(mediacontroller);
            videoView.setVideoURI(uri);
            videoView.seekTo(1);
        
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
    
        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                videoView.start();
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoView.resume();
            }
        });
    }
}
