package com.activity_result;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.rntryouts.R;

public class SampleActivity extends AppCompatActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
        findViewById(R.id.ok).setOnClickListener((view)->{
            Intent intent=new Intent();
            setResult(Activity.RESULT_OK,intent);
            finish();
            
        });
        findViewById(R.id.cancel).setOnClickListener((view)->{
            Intent intent = new Intent();
            setResult(Activity.RESULT_CANCELED,intent);
            finish();
        });
    }
    
}
