package com.activity_result;

import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class ActivityResultModule extends ReactContextBaseJavaModule {
    private ReactApplicationContext reactContext;
    private Promise mPromise;
    
    public ActivityResultModule(ReactApplicationContext context){
        super(context);
        reactContext = context;
        reactContext.addActivityEventListener(mActivityEventListener);
    }
    
    private ActivityEventListener mActivityEventListener=new BaseActivityEventListener(){
        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
            super.onActivityResult(activity, requestCode, resultCode, data);
            if(requestCode==1){
                if(Activity.RESULT_OK==resultCode){
                    mPromise.resolve("Resolved");
                }
                else{
                    mPromise.reject("1","Not selected");
                }
            }
        }
    };
    
    @NonNull
    @Override
    public String getName() {
        return "ActivityResult";
    }

    @ReactMethod
    public void startSampleActivity(Promise promise){
        mPromise = promise;
        Activity currentActivity=getCurrentActivity();
        Intent intent = new Intent(currentActivity,SampleActivity.class);
        currentActivity.startActivityForResult(intent,1);
    }
    
    private void sendEvent(ReactContext reactContext, String eventName, @Nullable WritableMap parms){
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName,parms);
    }
}
