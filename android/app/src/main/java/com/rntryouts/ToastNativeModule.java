package com.rntryouts;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.util.HashMap;
import java.util.Map;

public class ToastNativeModule extends ReactContextBaseJavaModule {
    private static final String DURATION_SHORT_KEY = "SHORT";
    private static final String DURATION_LONG_KEY = "LONG";
    private ReactApplicationContext reactContext;
    
    public ToastNativeModule(ReactApplicationContext context){
        super(context);
        reactContext = context;
        
    }
    
    @NonNull
    @Override
    public String getName() {
        return "ToastMaker";
    }
    
    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        final Map<String,Object> map = new HashMap<>();
        map.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
        map.put(DURATION_SHORT_KEY,Toast.LENGTH_SHORT);
        return map;
    }
    
    @ReactMethod
    public void show(String message, int duration){
        Toast.makeText(getReactApplicationContext(), message, duration).show();
        WritableMap params = Arguments.createMap();
        params.putString("message","Test message:"+String.format("%.2f",Math.random()));
        sendEvent(reactContext,"NativeEvent",params);
    }
    
    private void sendEvent(ReactContext reactContext, String eventName, @Nullable  WritableMap parms){
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName,parms);
    }
}
