import PropTypes from 'prop-types';

import { requireNativeComponent, ViewPropTypes } from 'react-native';

var viewProps = {
  name: 'CustomVideoView',
  propTypes: {
    url: PropTypes.string,
    ...ViewPropTypes,
  }
}
module.exports = requireNativeComponent('CustomVideoView', viewProps);