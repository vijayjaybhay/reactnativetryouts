import React, { Component } from "react";
import {
    View,
    Button,
    Text,
  } from 'react-native';

import ToastMaker from "./native_module/ToastMaker";
import ActivityResult from "./native_module/ActivityResult";
import CustomVideoView from "./native_module/CustomVideoView";
import {NativeEventEmitter,NativeModules} from "react-native";
const eventEmitter = new NativeEventEmitter(NativeModules.ToastMaker);
const EVENT_NATIVE="NativeEvent";
export class Home extends Component{
    constructor(props){
        super(props);
        this.state={
            activityResult:''
        }
    }
    componentDidMount(){
        eventEmitter.addListener(EVENT_NATIVE,(event)=>{
            console.log("Received value:"+event.message);
            this.state.activityResult=event.message;
            this.setState(this.state);
        });
    }
    render(){
        return(
            <View style={{flex:1, flexDirection:'column'}} >
              <Button 
                      title="Show toast" 
                      onPress={()=>{ this.showToast();}}
              />
              <Button 
                    title="Start Activity For Result" 
                    onPress={()=>{this.handleActivityResult()}}
                />
             <Text style={{alignSelf:'center'}}>Recieved Event from Native Code:</Text>
             <Text style={{alignSelf:'center',marginBottom:32}}>{this.state.activityResult}</Text>
             <View 
                style={{ flex: 1, justifyContent: 'center', alignItems: 'center',  }}>
                    <CustomVideoView 
                        style={{width: '100%', height: 200,alignSelf:'center' }} 
                        url="https://www.radiantmediaplayer.com/media/bbb-360p.mp4"
                        />
                </View>
            </View>
        );
    }

    showToast(){
        ToastMaker.show("Test toast message",ToastMaker.LONG);
    }

    async handleActivityResult(){
        try{
            let result = await ActivityResult.startSampleActivity();
            console.log("ActivityResult:OK-"+result);
          }catch(e){
            console.log("ActivityResult:Cancelled- Code:"+e.code+" Message:"+e.message);
          }
    }

    // componentWillMount(){
    //     eventEmitter.removeListener(EVENT_NATIVE);
    // }
}